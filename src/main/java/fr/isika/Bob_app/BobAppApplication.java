package fr.isika.Bob_app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.isika.Bob_app.dao.PreferenceRepository;
import fr.isika.Bob_app.dao.UtilisateurRepository;
import fr.isika.Bob_app.model.Preference;
import fr.isika.Bob_app.model.Utilisateur;

//testttt22222222
@SpringBootApplication
public class BobAppApplication implements CommandLineRunner{

	@Autowired
	private UtilisateurRepository utilisateurRepo;
	@Autowired
	private PreferenceRepository preferenceRepo;
	
	
	public static void main(String[] args) {
		SpringApplication.run(BobAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
	}

}
