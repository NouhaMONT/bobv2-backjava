package fr.isika.Bob_app.request;


import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class SignupRequest {
	
	@NotBlank
	@Size(min=3, max=20)
	private String username; 
	
	@NotBlank
	@Size(min=3, max=20)
	private String nom; 
	
	@NotBlank
	@Size(min=3, max=20)
	private String prenom; 
	
	@NotBlank
	@Size(max=50)
	private String email; 
	
	private String role; 
	
	@NotBlank
	@Size(min=5, max=20)
	private String motDepass;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getMotDepass() {
		return motDepass;
	}
	public void setMotDepass(String motDepass) {
		this.motDepass = motDepass;
	}
	
	

}
