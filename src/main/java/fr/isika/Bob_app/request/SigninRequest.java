package fr.isika.Bob_app.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class SigninRequest {
	
	@NotBlank
	@Size(min=3, max = 60)
	private String username; 
	
	@NotBlank
	private String motDepass;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getMotDepass() {
		return motDepass;
	}
	
	public void setMotDepass(String motDepass) {
		this.motDepass = motDepass;
	}
	

}
