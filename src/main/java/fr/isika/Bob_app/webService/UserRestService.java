package fr.isika.Bob_app.webService;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.Bob_app.dao.UtilisateurRepository;
import fr.isika.Bob_app.domaine.JwtResponse;
import fr.isika.Bob_app.domaine.Message;
import fr.isika.Bob_app.model.Utilisateur;
import fr.isika.Bob_app.request.SigninRequest;
import fr.isika.Bob_app.request.SignupRequest;
import fr.isika.Bob_app.security.JwtTokenUtil;
import fr.isika.Bob_app.service.MyUserDetails;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/authenticate")
public class UserRestService {
	@Autowired
	private UtilisateurRepository userRepo;
	
	@Autowired
	  PasswordEncoder encoder;
	
	@Autowired
	  AuthenticationManager authenticationManager;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	 @GetMapping(path="/users")
	public List<Utilisateur> getUsers(){
		return userRepo.findAll();
	}

	@RequestMapping(value="/users/{id}", method=RequestMethod.GET)
	public Optional<Utilisateur> getUser(@PathVariable Long id){
		return userRepo.findById(id);
	}
	
//	@RequestMapping(value="/users", method=RequestMethod.POST)
//	public Utilisateur save(@RequestBody Utilisateur p){
//		return userRepo.save(p);
//	}
	
	@RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		userRepo.deleteById(id);
		return true;
	}
	
	
	@RequestMapping(value="/users/{id}", method=RequestMethod.PUT)
	public Utilisateur update(@PathVariable Long id, @RequestBody Utilisateur p){
		p.setId(id);
		return userRepo.save(p);
	}
	
	@RequestMapping(value="/chercherUsers", method=RequestMethod.GET)
	public List<Utilisateur> chercher(@RequestParam(name="mc", defaultValue = "")String mc){
		return userRepo.findByNomContains(mc);
	}
	
//	@RequestMapping(value="/userByEmail/{email}", method=RequestMethod.GET)
//	public Utilisateur chercherParLogin(@RequestParam(name="email", defaultValue = "")String email){
//		return userRepo.findByEmail(email);
//	}
	
	@RequestMapping(value="/userByEmail/{email}", method=RequestMethod.GET)
	  public   ResponseEntity<Utilisateur> getUtilisateurByEmail(@PathVariable String email) {
		  Utilisateur Utilisateur = userRepo.findByEmail(email);
		   return ResponseEntity.ok().body(Utilisateur);
	  } 
	
//	@RequestMapping(value="/userByLog", method=RequestMethod.GET)
//	public Utilisateur chercherParLogin(@RequestParam(name="login", defaultValue = "")String login){
//		return userRepo.findByLogin(login);
//	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> loginUser(@Valid @RequestBody SigninRequest data ){
		Authentication authentication =authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
								data.getUsername(),
								data.getMotDepass())
						);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtTokenUtil.generateJwtToken(authentication);
		
		//UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		 
	    //return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
		MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getNom(),
				userDetails.getPrenom(),
				userDetails.getEmail(),
				userDetails.getRole()));
		// UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		 
		    //return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
//		
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest data){
		if (userRepo.existsByUsername(data.getUsername())) {
			return new ResponseEntity<>(new Message("Error: Username is already taken!"),
					 HttpStatus.BAD_REQUEST);
		}	
		if (userRepo.existsByEmail(data.getEmail())) {
			return new ResponseEntity<>(new Message("Error: Email is already in use!"),
					 HttpStatus.BAD_REQUEST);
		}
		//create new user's account
		Utilisateur user = new Utilisateur()
				.withUserName(data.getUsername())
				.withNom(data.getNom())
				.withPrenom(data.getPrenom())
				.withEmail(data.getEmail())
				.withEncodedPassword(encoder.encode(data.getMotDepass()))
				.withRole(data.getRole());
		userRepo.save(user);
		return new ResponseEntity<>(new Message ("User registered successfuly!"),HttpStatus.OK ); 
	}
}
