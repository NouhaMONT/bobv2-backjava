package fr.isika.Bob_app.webService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.Bob_app.dao.PreferenceRepository;
import fr.isika.Bob_app.model.Preference;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", allowCredentials = "true")
@RequestMapping("/api/authenticate")
public class PreferenceRestService {

	@Autowired
	private PreferenceRepository prefRepo; 
	
	@RequestMapping(value="/prefrences", method=RequestMethod.GET)
	public List<Preference> getPreference(){
		return prefRepo.findAll();
	}
	

	@RequestMapping(value="/prefrences/{id}", method=RequestMethod.GET)
	public Optional<Preference> getPrefById(@PathVariable Long id){
		return prefRepo.findById(id);
	}
	
	@RequestMapping(value="/prefrences", method=RequestMethod.POST)
	public Preference save(@RequestBody Preference p) {
		return prefRepo.save(p);
	}
	
	@RequestMapping(value="/prefrences/{id}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		prefRepo.deleteById(id);
		return true;
	}
	
	
}
