package fr.isika.Bob_app.domaine;

public class JwtResponse {
	private String jwt ; 
	private Long id; 
	private String username; 
	private String nom; 
	private String prenom;
	private String email; 
	private String role;
	public String getJwt() {
		return jwt;
	}
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public JwtResponse(String jwt, Long id, String username, String nom, String prenom, String email, String role) {
		
		this.jwt = jwt;
		this.id = id;
		this.username = username;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
	} 
	
//	private String token;
//	  private String type = "Bearer";
//	  private String username;
//	  private Collection<? extends GrantedAuthority> authorities;
//	 
//	  public JwtResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities) {
//	    this.token = accessToken;
//	    this.username = username;
//	    this.authorities = authorities;
//	  }
//	 
//	  public String getAccessToken() {
//	    return token;
//	  }
//	 
//	  public void setAccessToken(String accessToken) {
//	    this.token = accessToken;
//	  }
//	 
//	  public String getTokenType() {
//	    return type;
//	  }
//	 
//	  public void setTokenType(String tokenType) {
//	    this.type = tokenType;
//	  }
//	 
//	  public String getUsername() {
//	    return username;
//	  }
//	 
//	  public void setUsername(String username) {
//	    this.username = username;
//	  }
//	  
//	  public Collection<? extends GrantedAuthority> getAuthorities() {
//	    return authorities;
//	  }
}
