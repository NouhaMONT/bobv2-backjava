package fr.isika.Bob_app.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Preference implements Serializable  {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	private String ville_metier; 
	private String code_rome; 
	private Double longitude;
	private Double latitude;
	
	

	//getter+settter
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVille_metier() {
		return ville_metier;
	}
	public void setVille_metier(String ville_metier) {
		this.ville_metier = ville_metier;
	}
	public String getCode_rome() {
		return code_rome;
	}
	public void setCode_rome(String code_rome) {
		this.code_rome = code_rome;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	//constructor
//	public Preference(Long id, String ville_metier, String code_rome, Double longitude, Double latitude) {
//		super();
//		this.id = id;
//		this.ville_metier = ville_metier;
//		this.code_rome = code_rome;
//		this.longitude = longitude;
//		this.latitude = latitude;
//	}
	
//	public Preference() {
//		super();
//	}
	
	//Méthode tostring 
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Preference [id=");
		builder.append(id);
		builder.append(", ville_metier=");
		builder.append(ville_metier);
		builder.append(", code_rome=");
		builder.append(code_rome);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append("]");
		return builder.toString();
	} 
	
}
