package fr.isika.Bob_app.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "Utilisateur", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
		@UniqueConstraint(columnNames = { "email" }) })
@Data
@ToString

public class Utilisateur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2803263400466566564L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank
	@Size(min = 3, max = 20)
	private String username;

	private String nom;

	private String prenom;

	@NotBlank
	@Size(min = 3, max = 20)
	private String email;

	@NotBlank
	private String motDepass;
	private String role;

	// constructor

	public Utilisateur(String username, String nom, String prenom, String email, String motDepass, String role) {
		super();
		this.username = username;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.motDepass = motDepass;
		this.role = role;
	}

	public Utilisateur withUserName(final String username) {
		this.username = username;
		return this;
	}
	
//	public Utilisateur(Long id, String nom, String prenom, String email, String motDepass, String role) {
//		super();
//		this.id = id;
//		this.nom = nom;
//		this.prenom = prenom;
//		this.email = email;
//		this.motDepass = motDepass;
//		this.role = role; 
//	}

	public Utilisateur() {

	}

	// getter+setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDepass() {
		return motDepass;
	}

	public void setMotDepass(String motDepass) {
		this.motDepass = motDepass;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Utilisateur withEmail(String email) {
		this.email = email;
		return this;
	}

	public Utilisateur withNom(String nom2) {
		nom = nom2;
		return this;
	}

	public Utilisateur withPrenom(String prenom2) {
		prenom = prenom2;
		return this;
	}

	public Utilisateur withEncodedPassword(String encodedPassword) {
		motDepass = encodedPassword;
		return this;
	}

	public Utilisateur withRole(String role2) {
		role = role2;
		return this;
	}

	// methode tostring
//	@Override
//	public String toString() {
//		StringBuilder builder = new StringBuilder();
//		builder.append("Utilisateur [id=");
//		builder.append(id);
//		builder.append(", nom=");
//		builder.append(nom);
//		builder.append(", prenom=");
//		builder.append(prenom);
//		builder.append(", email=");
//		builder.append(email);
//		builder.append(", motDepass=");
//		builder.append(motDepass);
//		builder.append(", role=");
//		builder.append(role);
//		builder.append("]");
//		return builder.toString();
//	}

}
