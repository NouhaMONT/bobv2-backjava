package fr.isika.Bob_app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.Bob_app.dao.UtilisateurRepository;
import fr.isika.Bob_app.model.Utilisateur;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UtilisateurRepository userRepo; 
	
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Utilisateur> user =userRepo.findByUsername(username);
		
		user.orElseThrow(()-> new UsernameNotFoundException(username + "not found. " ));
		
		return user.map(MyUserDetails::new).get();
	}

}
