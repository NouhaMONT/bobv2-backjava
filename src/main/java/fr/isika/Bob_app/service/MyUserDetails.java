package fr.isika.Bob_app.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.isika.Bob_app.model.Utilisateur;

public class MyUserDetails implements UserDetails {
	
	private static final long serialVersionUID = 1L; 
	
	private Long id; 
	private String username; 
	private String email; 
	
	@JsonIgnore
	private String password; 
	private String nom; 
	private String prenom; 
	private boolean isActive;
	private String role; 
	private List<GrantedAuthority> authorities; 
	
	 public MyUserDetails(Utilisateur user) {
		 this.id = user.getId(); 
		 this.email = user.getEmail(); 
		 this.role = user.getRole(); 
		 this.nom = user.getNom(); 
		 this.prenom = user.getPrenom(); 
		this.username = user.getUsername(); 
		this.password =user.getMotDepass();
		this.authorities = Arrays.stream(user.getRole().split(","))
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}
	
	 public MyUserDetails() {}
	
	
	
	
	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		
		return password;
	}

	@Override
	public String getUsername() {
		
		return username;
	}
	
	public Long getId() {
		
		return id;
	}
	
   public String getEmail() {
		
		return email;
	}
   
   public String getNom() {
	return nom;
}
   
   public String getPrenom() {
	return prenom;
}
   
   public String getRole() {
		
		return role;
	}

	@Override
	public boolean isAccountNonExpired() {
		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
	
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return true;
	}

}
