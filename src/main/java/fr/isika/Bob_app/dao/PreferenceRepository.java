package fr.isika.Bob_app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import fr.isika.Bob_app.model.Preference;


@CrossOrigin("*")
@RepositoryRestResource
public interface PreferenceRepository extends JpaRepository<Preference,Long> {
		
	  
}
