package fr.isika.Bob_app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;


import fr.isika.Bob_app.model.Utilisateur;

@CrossOrigin("*")
@RepositoryRestResource
public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long> {
	   @RestResource(path="/userByNom")
	   public List<Utilisateur> findByNomContains(@Param("mc")String mc);
	   
	   @RestResource(path="/userByEmail")
	   public Utilisateur findByEmail(@Param("email")String email);
	   
	   Optional<Utilisateur> findByUsername(String username);
	   
	   Boolean existsByUsername (String username); 
	   Boolean existsByEmail (String email); 
}
